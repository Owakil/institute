<?php

include_once 'vendor/autoload.php';

use Pondit\Institute\Student;
use Pondit\Institute\Subjects;
use Pondit\Institute\Mark;
use Pondit\Institute\Teacher;
use Pondit\Institute\Group;

$student1 = new Student();
var_dump($student1);

$subjects1 = new Subjects();
var_dump($subjects1);

$mark1 = new Mark();
var_dump($mark1);

$teacher1 = new Teacher();
var_dump($teacher1);

$group1 = new Group();
var_dump($group1);

